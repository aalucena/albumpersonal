
angular.module('starter').controller('MainCtrl', function($scope, $timeout, $ionicPopup, ionicMaterialMotion, ionicMaterialInk, $ionicScrollDelegate) {
	$ionicPopup.alert({
		title: 'Bienvenid@',
		template: 'Conectar GPS para tener toda la funcionalidad de la app'
	}).then(function(res) {
		console.log('error');
	});
	

    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 20);

    $timeout(function() {
        ionicMaterialMotion.fadeSlideInRight({
            startVelocity: 3000
        });
    }, 700);

    // Set Ink
    ionicMaterialInk.displayEffect();
	$scope.allImages = [
	  {
		src: 'img/1.jpg'
	  },
	  {
		src: 'img/2.jpg'
	  }, 
	  {
		src: 'img/3.jpg'
	  }, 
	  {
		src: 'img/4.jpg'
	  }, 
	  {
		src: 'img/5.jpg'
	  }, 
	  {
		src: 'img/6.jpg'
	  }
	];
  
})