
angular.module('starter').controller('FotografiarCtrl', function($scope, $cordovaCamera, $cordovaFile, $filter, $cordovaSQLite, $ionicPopup) {
  var sourceDirectory='';
  var newFolder='';
  var folderName='';
  this.url='';
  this.longitud='';
  this.latitud='';
  this.date='';
  $scope.getPhoto = function() {
	  
	$cordovaFile.createDir(cordova.file.externalRootDirectory, "albumPersonal", true)
		  .then(function (success) {
			newFolder=cordova.file.externalRootDirectory+'albumPersonal'+'/'; 
			var d = new Date();
			var month = new Array();
			month[0] = "Enero";
			month[1] = "Febrero";
			month[2] = "Marzo";
			month[3] = "Abril";
			month[4] = "Mayo";
			month[5] = "Junio";
			month[6] = "Julio";
			month[7] = "Agosto";
			month[8] = "Septiembre";
			month[9] = "Octubre";
			month[10] = "Noviembre";
			month[11] = "Diciembre";
			folderName = month[d.getMonth()];
			console.log("folderName "+folderName); 
			$cordovaFile.createDir(newFolder, folderName, true)
				.then(function (success) {
					// success
				}, function (error) {
					// error
				});
			// success
		  }, function (error) {
			// error
		  });
	
	var options = {
	  quality: 100,
	  destinationType: Camera.DestinationType.FILE_URI,
	  sourceType: Camera.PictureSourceType.CAMERA,
	  encodingType: Camera.EncodingType.JPEG,
	  cameraDirection: 1,
	  saveToPhotoAlbum: false
	};
	 
	$cordovaCamera.getPicture(options).then(function(imagePath){
		console.log(imagePath);
		$scope.lastPhoto = imagePath;
		navigator.geolocation.getCurrentPosition(function (pos) {
			console.log('Got pos', pos);
			//alert(pos.coords.latitude + ',' + pos.coords.longitude);
			this.longitud=pos.coords.longitude;
			this.latitud=pos.coords.latitude;
			
			sourceDirectory = imagePath.substring(0, imagePath.lastIndexOf('/') + 1);
			var sourceFileName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.length);
			console.log("sourceFileName "+sourceFileName);
			var d = new Date(),
				n = d.getTime(),
				newFileName = n + ".jpg";
			console.log("newFileName "+newFileName); 
			console.log("sourceDirectory "+sourceDirectory); 
		
			var newDirectory=newFolder+folderName+'/';
			console.log("newDirectory "+newDirectory); 
			//Move the file to permanent storage
			$cordovaFile.moveFile(sourceDirectory, sourceFileName, newDirectory, newFileName).then(function(success){
				//success.toURL() es la ubicacion final de la foto tomada: success.toURL() file:///storage/sdcard0/albumPersonal/Marzo/1459108030206.jpg
				console.log("success.toURL() "+success.toURL());
				this.url=success.toURL();
				this.date = $filter('date')(Date.now(), 'yyyy-MM-dd');
				console.log('url', this.url);
				console.log('longitud', this.longitud);
				console.log('latitud', this.latitud);
				console.log('date', this.date);
				var titulo='Titulo';
				var comentario='Comentario';
				var query = "INSERT INTO foto (titulo, comentario, url, longitud, latitud, fecha) VALUES (?,?,?,?,?,?)";
				$cordovaSQLite.execute(db, query, [titulo, comentario, url, longitud, latitud, date]).then(function(res) {
					console.log("INSERT ID -> " + res.insertId);
					$ionicPopup.alert({
						title: 'Foto guardada en la BBDD'
					}).then(function(res) {
						console.log('Foto guardada');
					});
					//alert("Foto insertada en la BBDD");
				}, function (err) {
					$ionicPopup.alert({
						title: 'Foto no guardada',
						template: 'Error al guardarla en el dispositivo'
					}).then(function(res) {
						console.log('error inserción BBDD');
						console.error(err);
					});
				});
			}, function(error){
				$ionicPopup.alert({
					title: 'Foto no guardada',
					template: 'Error al guardarla en el dispositivo'
				}).then(function(res) {
					console.log('error mover fichero');
				});
			});
			
		}, function (error) {
			$ionicPopup.alert({
				title: 'Foto no guardada',
				template: 'No se puede conseguir la localizacion'
			}).then(function(res) {
				console.log('error localizacion');
			});
			//alert('No se puede conseguir la localizacion: ' + error.message);
		});		
	}, function(error){
		$ionicPopup.alert({
			title: 'Foto no realizada',
			template: 'Error al realizar la foto'
		}).then(function(res) {
			console.log('error foto no realizada');
		});
	});	
  };
  
});
