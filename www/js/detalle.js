
angular.module('starter').controller('DetalleCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk, $cordovaSQLite, $ionicPopup, $cordovaFile, $cordovaSocialSharing) {
	
	this.parametro=$stateParams.id;
	var query = "SELECT * FROM foto WHERE id = ?";
        $cordovaSQLite.execute(db, query, [this.parametro]).then(function(res) {
            if(res.rows.length > 0) {
                console.log("SELECTED -> " + res.rows.item(0).titulo + " " + res.rows.item(0).comentario + " " + res.rows.item(0).url);
                //alert("SELECTED -> " + res.rows.item(0).titulo + " " + res.rows.item(0).comentario + " " + res.rows.item(0).url);
                $scope.titulo = res.rows.item(0).titulo;
				$scope.comentario = res.rows.item(0).comentario;
                $scope.url = res.rows.item(0).url;
				$scope.longitud = res.rows.item(0).longitud;
				$scope.latitud = res.rows.item(0).latitud;
				$scope.fecha = res.rows.item(0).fecha;
            } else {
                console.log("Ningún resultado");
                $ionicPopup.alert({
					title: 'Ningún resultado'
				}).then(function(res) {
					console.log('error');
				});
				//alert("Ningún resultado");
            }
        }, function (err) {
            console.error(err);
        });
		
	$timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 20);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
	$scope.getDatos = function() {
		$ionicPopup.prompt({
			title: 'Editar Título',
			template: 'Introduce nuevo título',
			inputType: 'text',
			inputPlaceholder: 'Titulo'
		}).then(function(res) {
			console.log('El nuevo título es: ', res);
			$scope.titulo=res;
			$ionicPopup.prompt({
				title: 'Editar comentario',
				template: 'Introduce nuevo comentario',
				inputType: 'text',
				inputPlaceholder: 'Comentario'
			}).then(function(res) {
				console.log('El nuevo comentario es: ', res);
				$scope.comentario=res;
				var id=$stateParams.id;
				db.transaction(function(tx){
					var query= "UPDATE foto SET titulo= ?, comentario= ?  WHERE id = ?";
						tx.executeSql(query, [$scope.titulo, $scope.comentario, id],function SuccessUpdate(tx,result){			
							$ionicPopup.alert({
								title: 'Edición foto',
								template: 'Datos modificados correctamente'
							}).then(function(res) {
								console.log('Modificación OK');
							});
							//alert("Datos modificados correctamente");
						},
						function errorUpdate(error){
							$ionicPopup.alert({
								title: 'Edición foto',
								template: 'Error modificación datos'
							}).then(function(res) {
								console.log('error');
							});
						});
				});   
			});
		});
		
	 
	};
	$scope.borrarFoto = function() {
		var id=$stateParams.id;
		$ionicPopup.confirm({
			 title: 'Eliminar foto',
			 template: '¿Estás seguro de que quieres eliminarla?'
			 
		}).then(function(res) {
			if(res) {
				db.transaction(function(tx){
					var query= "DELETE FROM foto WHERE id = ?";
					tx.executeSql(query, [id],function success(tx,result){
						$ionicPopup.alert({
							title: 'Foto eliminada'
						}).then(function(res) {
							console.log('Eliminación OK');
						});
						//alert("Foto eliminada");
					},
					function error(error){
						$ionicPopup.alert({
							title: 'Error eliminación foto'
						}).then(function(res) {
							console.log('error');
						});
						//alert("Error processing SQL : "+error.code);
					});
				});   
				var path = $scope.url.substring(0, $scope.url.lastIndexOf('/') + 1);
				var file = $scope.url.substring($scope.url.lastIndexOf('/') + 1, $scope.url.length);
				$cordovaFile.removeFile(path, file).then(function(success){
					console.log('Foto borrada');
				}, function(error){
					//an error occured
				});	
			} else {
				$ionicPopup.alert({
					title: 'Foto no eliminada'
				}).then(function(res) {
					console.log('error');
				});
			}
		});		
	};
	$scope.enviarWhatsApp = function(){
		var message='Mensaje via WhatsApp';
		
		$cordovaSocialSharing
			.shareViaWhatsApp(message, $scope.url, null)
			.then(function(result) {
				// Success!
			}, function(err) {
				// An error occurred. Show a message to the user
			});
	};
})