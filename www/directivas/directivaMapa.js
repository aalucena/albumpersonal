
angular.module('starter.directivaMapa', [])

.directive('map', function() {
  return {
    restrict: 'E',
    scope: {
      onCreate: '&'
    },
    link: function ($scope, $element, $attr) {
		var myLatlng=new google.maps.LatLng($attr.latitud, $attr.longitud);
		console.log('longitud: ', $attr.longitud);
		console.log('latitud: ', $attr.latitud);
		function initialize() {
			var mapOptions = {
				center: myLatlng,
				zoom: 14,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
		
			var map = new google.maps.Map($element[0], mapOptions);
			$scope.onCreate({map: map});
		
			var infowindow = new google.maps.InfoWindow();
			var marker, i;
			marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				animation: google.maps.Animation.DROP,
				title: "Event is here"
			});
			
			$scope.map = map;
			// Stop the side bar from dragging when mousedown/tapdown on the map
			google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
				e.preventDefault();
				return false;
			});
		
		}

		if (document.readyState === "complete") {
			initialize();
		} else {
			google.maps.event.addDomListener(window, 'load', initialize);
		}
    }
  }
});
