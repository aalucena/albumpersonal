
angular.module('starter').controller('ListadoCtrl', function($scope, $cordovaSQLite, $timeout, ionicMaterialInk, ionicMaterialMotion, $ionicPopup) {
	$scope.select = function() {
		$scope.acontecimientosSqlite=[];
		var query = "SELECT id, titulo, url, fecha FROM foto";
        $cordovaSQLite.execute(db, query, []).then(function(res) {
            if(res.rows.length > 0) {
                console.log();
                //alert("SELECTED -> " + res.rows.item(0).id + " " + res.rows.item(0).titulo + " " + res.rows.item(0).url + " " + res.rows.item(0).fecha);
                for (i = 0; i < res.rows.length; i++) {
					$scope.id = res.rows.item(i).id;
					$scope.titulo = res.rows.item(i).titulo;
					$scope.url = res.rows.item(i).url;
					$scope.fecha = res.rows.item(i).fecha;
					$scope.acontecimientosSqlite.push({"id":$scope.id, "titulo":$scope.titulo, "url":$scope.url, "fecha":$scope.fecha});
				}
				
            } else {
                console.log("Ningún resultado");
				$ionicPopup.alert({
					title: 'No hay ninguna foto guardada'
				}).then(function(res) {
					console.log('error');
				});
                //alert("Ningun resultado en la base de datos");
            }
        }, function (err) {
            console.error(err);
        });
	};
	// Animate list on this event
    $scope.$on('ngLastRepeat.mylist',function(e) {
        $timeout(function(){
            ionicMaterialMotion.fadeSlideInRight();
            ionicMaterialInk.displayEffect();
          },0); // No timeout delay necessary.
    });
	
})