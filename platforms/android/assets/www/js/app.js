
var db = null;
angular.module('starter', ['ionic', 'starter.directivaMapa', 'starter.directivaMaterialIonic', 'ionic-material', 'ngCordova', 'ngCordova.plugins.socialSharing'])

.run(function($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
	document.addEventListener('deviceready', onDeviceReady, false);
	// Cordova is ready
	function onDeviceReady() {
		//Esta línea debe comentarse, si no, se borraría la BBDD cada vez que se inicia la aplicación
		//window.sqlitePlugin.deleteDatabase({name: 'albumPersonal.db', iosDatabaseLocation: 'default'});
		db = window.sqlitePlugin.openDatabase({name: 'albumPersonal.db', iosDatabaseLocation: 'default'}, function(db) {
			db.transaction(function(tx) {
				$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS foto (id INTEGER PRIMARY KEY, titulo TEXT, comentario TEXT, url TEXT, longitud DECIMAL, latitud DECIMAL, fecha DATE)");
			}, function(err) {
				console.log('Open database ERROR: ' + JSON.stringify(err));
			});
		});			
	}
  });
})

.config(function($stateProvider, $urlRouterProvider, $compileProvider, $ionicConfigProvider) {
	$ionicConfigProvider.tabs.position('bottom');
	$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);

	$stateProvider

    .state('tab', {
		url: '/tab',
		abstract: true,
		templateUrl: 'templates/tabs.html'
	})

	.state('tab.inicio', {
		url: '/inicio',
		views: {
		  'tab-inicio': {
			templateUrl: 'templates/tab-inicio.html',
			controller: 'MainCtrl'
		  }
		}
	})

	.state('tab.listado', {
		url: '/listado',
		views: {
			'tab-listado': {
			  templateUrl: 'templates/tab-listado.html',
			  controller: 'ListadoCtrl'
			}
		}
    })
    .state('tab.detalle', {
		url: '/listado/:id',
		views: {
			'tab-listado': {
			  templateUrl: 'templates/listado-detalle.html',
			  controller: 'DetalleCtrl'
			}
		}
    })
	.state('tab.mapa', {
		url: '/listado/mapa/:longitud/:latitud',
		views: {
			'tab-listado': {
				templateUrl: 'templates/mapa.html',
				controller: 'mapaController',
				controllerAs: 'mapaCtrl'
			}
		}
    })

	.state('tab.galeria', {
		url: '/galeria',
		views: {
		  'tab-galeria': {
			templateUrl: 'templates/tab-galeria.html',
			controller: 'GaleriaCtrl'
		  }
		}
	})
	  
	.state('tab.fotografiar', {
		url: '/fotografiar',
		views: {
		  'tab-fotografiar': {
			templateUrl: 'templates/tab-fotografiar.html',
			controller: 'FotografiarCtrl'
		  }
		}
	});
	
	$urlRouterProvider.otherwise('/tab/inicio');

});
